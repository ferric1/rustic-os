use thiserror::Error;

#[derive(Error, Debug)]
pub enum FSError {
    #[error("Error Serilizing data with error: {0}")]
    SerializationError(#[from] bincode::Error),
}