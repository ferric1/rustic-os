use crate::FileSystem;
use crate::traits::Format;
use anyhow::Result;
use rustic_disk::Disk;
use rustic_disk::traits::BlockStorage;
use crate::dir_entry::{Block, DirEntry, FileType};

impl Format for FileSystem {
    fn format(&mut self) -> Result<()>{
        // disk should always exist since we handle making a dsk in the constructor
        if Disk::disk_exists() {
            Disk::delete_disk()?;
        }

        let blk = Block {
            parent_entry: DirEntry {
                name: "/".to_string(),
                file_type: FileType::Directory,
                ..Default::default()
            },
            blk_num: 0,
            entries: vec![DirEntry::default(); 64],
        };

        self.disk = Disk::new()?;
        self.disk.write_block(0, &blk)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_format() -> Result<()> {
        let mut fs = FileSystem::new()?;
        fs.format()?;
        assert!(Disk::disk_exists());

        // read the first block and check if it's a directory
        let block: Block = fs.read_blk(0)?;
        assert_eq!(block.parent_entry.file_type, FileType::Directory);

        Ok(())
    }
}