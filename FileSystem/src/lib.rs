mod utils;
pub mod traits;
mod dir_entry;
mod format;
mod errors;

use rustic_disk::Disk;
use rustic_disk::traits::BlockStorage;
use anyhow::Result;
use serde::Serialize;
use serde_derive::Deserialize;
use crate::dir_entry::{Block, DirEntry, FileType};
use crate::errors::FSError;

pub struct FileSystem {
    disk: Disk,
    curr_block: Block,
    fat: Vec<FAT> // this is the amount of blocks in the disk
}

#[derive(Debug, Deserialize, Serialize)]
enum FAT {
    Free,
    Taken(u64),
    EOF,
}

impl FileSystem {
    pub fn new() -> Result<Self> {
        let disk = Disk::new()?;

        let (curr_block, fat) = if !Disk::disk_exists() {
            let fat = vec![FAT::Free; Disk::BLOCK_SIZE / std::mem::size_of::<Block>() as u64];
            let root_block = Block {
                parent_entry: DirEntry {
                    name: "/".to_string(),
                    file_type: FileType::Directory,
                    ..Default::default()
                },
                blk_num: 0,
                entries: vec![DirEntry::default(); 64],
            };
            disk.write_block(0, &root_block)?;
            disk.write_block(1, &fat)?;
            (root_block, fat)
        } else {
            let root_block: Block = disk.read_block(0)?;
            let fat: Vec<FAT> = disk.read_block(1)?;
            (root_block, fat)
        };


        Ok(FileSystem { disk, curr_block, fat })
    }

    pub fn write_curr_blk(&self) -> Result<()> {
        let block_to_write = self.curr_block.blk_num;
        self.disk.write_block(block_to_write as usize, &self.curr_block)?;
        Ok(())
    }

    pub fn write_data<T: Serialize>(&self, data: &T, start_blk: u64) -> Result<()> {
        // serilize the data and split into chunks
        let serialized_data = bincode::serialize(data).map_err(FSError::SerializationError)?;
        let chunks = serialized_data.chunks(Disk::BLOCK_SIZE);
        let mut blk = start_blk;
        for chunk in chunks {
            self.disk.write_serilized_data(blk as usize, chunk)?;
            blk += 1; // swap this to find next free block later
        }
        Ok(())
    }

    pub fn read_blk(&self, blk: u64) -> Result<Block> {
        let block: Block = self.disk.read_block(blk as usize)?;
        Ok(block)
    }
}

#[cfg(test)]
mod test {
    use crate::utils::current_timestamp;
    use super::*;

    #[test]
    fn test_file_system_creation() {
        let fs = FileSystem::new().unwrap();
        assert_eq!(fs.curr_block.blk_num, 0);
    }

    #[test]
    fn test_file_system_write_curr_blk() {
        let mut fs = FileSystem::new().unwrap();
        let entry = DirEntry {
            name: "test".to_string(),
            file_type: FileType::File,
            size: 0,
            created: current_timestamp(),
            modified: current_timestamp(),
        };
        fs.curr_block.entries.push(entry.clone());
        //fs.curr_block.entries[0] = entry.clone();
        fs.write_curr_blk().unwrap();
        let read_block = fs.read_blk(0).unwrap();
        assert_eq!(read_block.entries[0].name, entry.name);
    }
}