use anyhow::Result;

pub(crate) trait Format {
    fn format(&mut self) -> Result<()>;
}

pub(crate) trait File {
    fn create_file(&self, name: &str) -> Result<()>;
    fn delete_file(&self, name: &str) -> Result<()>;
    fn read_file(&self, name: &str) -> Result<()>;
    fn write_file(&self, name: &str) -> Result<()>;
}

pub(crate) trait Directory {
    fn create_dir(&self, name: &str) -> Result<()>;
    fn delete_dir(&self, name: &str) -> Result<()>;
}

pub(crate) trait DirEntryHandling {
    fn move_entry(&self, source: &str, dest: &str) -> Result<()>;
    fn copy_entry(&self, source: &str, dest: &str) -> Result<()>;
}