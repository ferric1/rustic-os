use env_logger::Builder;
use log::LevelFilter;

fn setup_logger() {
    Builder::new()
        .filter(None, LevelFilter::Debug)
        .init();
}

fn main() {
    setup_logger();
}
